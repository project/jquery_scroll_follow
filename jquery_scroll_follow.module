<?php

/**
 * @file
 * jQuery Scroll Follow is a simple module that use a jQuery plugin which allows
 * content to follow the page as the user scrolls.
 *
 * jquery.scrollfollow.js is the jQuery plugin which can be found here:
 * http://kitchen.net-perspective.com/open-source/scroll-follow/
 */

/**
 * jQuery Scroll Follow library default path.
 */
define('JQUERY_SCROLL_FOLLOW_DEFAULT_PATH', 'sites/all/libraries/jquery_scroll_follow');

/**
 * Implements hook_help().
 */
function jquery_scroll_follow_help($path, $arg) {
  switch ($path) {
    case 'admin/help#jquery_scroll_follow':
      return '<p>' . t('jQuery Scroll Follow is a simple module which allows blocks to follow the page as the user scrolls.') . '<p>';
      break;
  }
}

/**
 * Implements hook_menu().
 */
function jquery_scroll_follow_menu() {
  $items = array();

  $items['admin/config/user-interface/jquery_scroll_follow'] = array(
    'title' => 'jQuery Scroll Follow',
    'description' => 'Administer jQuery Scroll Follow settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('jquery_scroll_follow_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'jquery_scroll_follow.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_library().
 */
function jquery_scroll_follow_library() {

  $jquery_scroll_follow_location = variable_get('jquery_scroll_follow_location', JQUERY_SCROLL_FOLLOW_DEFAULT_PATH);

  $libraries['jquery_scroll_follow'] = array(
    'title' => 'jQuery Scroll Follow',
    'website' => 'http://kitchen.net-perspective.com/open-source/scroll-follow',
    'version' => '0.4.0',
    'js' => array(
      $jquery_scroll_follow_location . '/jquery.scrollfollow.js' => array(),
    ),
  );
  return $libraries;
}

/**
 * Implements hook_init().
 */
function jquery_scroll_follow_init() {

  global $base_path;

  $jquery_scroll_follow_location = variable_get('jquery_scroll_follow_location', JQUERY_SCROLL_FOLLOW_DEFAULT_PATH);

  if (file_exists(DRUPAL_ROOT . $base_path . $jquery_scroll_follow_location . '/jquery.scrollfollow.js')) {

    drupal_add_library('jquery_scroll_follow', 'jquery_scroll_follow');
    drupal_add_js(drupal_get_path('module', 'jquery_scroll_follow') . '/jquery_scroll_follow_start.js', array('group' => JS_DEFAULT, 'every_page' => TRUE));

    $settings = array(
      'jquery_scroll_follow_css_target' => variable_get('jquery_scroll_follow_css_target', ''),
      'jquery_scroll_follow_settings_speed' => variable_get('jquery_scroll_follow_settings_speed', 500),
      'jquery_scroll_follow_settings_offset' => variable_get('jquery_scroll_follow_settings_offset', 0),
      'jquery_scroll_follow_settings_delay' => variable_get('jquery_scroll_follow_settings_delay', 0),
    );

    drupal_add_js(array('jquery_scroll_follow' => $settings), 'setting');

  }
}
